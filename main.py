from panchayat import create_app

app = create_app()

if __name__ == "__main__":
    """
    Used for debugging/profiling.
    To profile install line_profiler.
    kernprof -l main.py
    python -m line_profiler main.py.lprof > profile.txt
    """
    app.run(
        host='0.0.0.0',
        debug=False,
        port=5000,
        use_reloader=False,  # without this get profile not defined
    )
