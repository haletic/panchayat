# Panchayat
A minimal hackernews style forum application derived off [flask tutorial](http://flask.pocoo.org/docs/1.0/tutorial)

## Sample
Try out the [sample](https://panchayat-sample.haletic.com) instance.  
Registration Token: keymaker  
The sample app is a sandbox. All changes are reverted nightly.  

## Run sample container locally
```
docker run -p 5000:80 registry.gitlab.com/haletic/panchayat:master
```
Any changes made in the application will be lost on container exit.

## Setup for persistent database

Panchayat stores its data in panchayat.yaml and its configuration in config.py.  
```
git clone https://gitlab.com/haletic/panchayat
cd panchayat
mkdir instance #instance directory contains configuration and database used by the app
#copy panchayat.yaml and config.py from sample_instance to instance folder
#modify config.py to change configuration options
#if panchayat.yaml is not provided app will create an empty database on first start
```

The instance directory can now be provided to panchayat running natively or inside docker.  

### Docker
```
docker build -t panchayat_dev .
docker run -p 5000:80 -v $(pwd)/instance:/app/instance panchayat_dev
The app should now be accessible at port 5000
```

### Native
```
python3.6 -m venv venv
. venv/bin/activate
pip install -r panchayat/requirements.txt
export FLASK_APP=panchayat
export FLASK_ENV=development

flask run --host=0.0.0.0
The app should now be accessible at port 5000
```
