import datetime

EXTERNAL_DOMAIN = 'haletic.com'
SLUG = 'panchayat-sample'
REGISTRATION_TOKEN = 'keymaker'
SITEMAP_MIN_LAST_MODIFIED = datetime.datetime(2021,3,6) # last modified time in the sitemap will be atleast this much
KHOJ_ENABLED = False
# KHOJ_URL = "http://localhost:8000"
