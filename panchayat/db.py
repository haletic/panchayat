"""
Wrappers to initialize and get db.
Before VDB, these functions would interact directly with SQL.
Now they wrap around VDB.
"""

import click
from flask import current_app
from flask.cli import with_appcontext
from panchayat.vdb import VDB


def get_db():
    """
    Return an instance of VDB
    """
    return current_app.config['VDB']


def init_db():
    """
    Create a fresh empty instance of VDB
    """
    vdb = VDB(outfile=current_app.config['DATABASE'])
    current_app.config['VDB'] = vdb
    vdb.commit()


@click.command('init-db')
@with_appcontext
def init_db_command():
    """CLI endpoint to init new db"""
    init_db()
    click.echo("Initialized the database.")


def init_app(app):
    """
    Initialize app
    """
    app.cli.add_command(init_db_command)
