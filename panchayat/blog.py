"""
Contains all panchayat homepage endpoints
"""

from copy import deepcopy

import urllib.parse
import requests

from flask import (Blueprint, current_app, flash, g, jsonify, redirect,
                   render_template, request, url_for, make_response)
from werkzeug.contrib.atom import AtomFeed
from werkzeug.exceptions import abort

from panchayat.auth import login_required, mailauth_required, token_required
from panchayat.db import get_db
from panchayat.helpers.mail_utils import broadcast_post
from panchayat.helpers.post_utils import reply
from panchayat.helpers.string_utils import is_empty
from panchayat.vdb import LinkPost, TextPost, Visibility

BP = Blueprint('blog', __name__)
ROOT_URL = current_app.config['ROOT_URL']


@BP.route('/')
def index():
    """
    End point for root homepage
    """
    vdb = get_db()
    return render_template('blog/index.html',
                           posts=vdb.posts,
                           root_url=ROOT_URL)


@BP.route('/activity')
def activity():
    """
    Endpoint for activity page
    """
    vdb = get_db()
    return render_template('blog/activity.html',
                           posts=vdb.posts,
                           root_url=ROOT_URL)


@BP.route('/search', methods=['GET', 'POST'])
@login_required
def search():
    """
    Endpoint for search page
    """
    # pylint: disable=eval-used, bare-except
    vdb = get_db()
    all_posts = deepcopy(
        vdb.posts.all())  # create copy so any change does not reach vdb
    queried_posts = []
    if request.method == 'POST':
        error = None
        filter_str = request.form.get("filter")
        try:
            filter_lambda = eval(f'lambda post: {filter_str}', {}, {})
        except:
            error = "Filter string is invalid"
        if error is None:
            sort_str = request.form.get("sort-key")
            try:
                sort_lambda = eval(f'lambda post: {sort_str}', {}, {})
            except:
                error = "Invalid sort key"
            if error is None:
                reverse = request.form.get("reverse") == "on"
                try:
                    filtered_posts = filter(filter_lambda, all_posts)
                except:
                    error = "Error while filtering"
                if error is None:
                    try:
                        sorted_posts = sorted(filtered_posts,
                                              key=sort_lambda,
                                              reverse=reverse)
                    except:
                        error = "Error while sorting"
                    if error is None:
                        queried_posts = sorted_posts
        if error is not None:
            flash(error)
    return render_template('blog/search.html',
                           posts=queried_posts,
                           root_url=ROOT_URL)


@BP.route('/khoj', methods=['GET', 'POST'])
@login_required
def khoj():
    """
    Endpoint for search page built on top of Khoj -
    (see https://github.com/debanjum/khoj/tree/saba/yaml-support)
    For more context, see https://gitlab.com/haletic/panchayat/-/issues/126.
    """
    khoj_enabled = current_app.config["KHOJ_ENABLED"]
    khoj_url = current_app.config["KHOJ_URL"]
    vdb = get_db()
    queried_posts = []

    if not khoj_enabled:
        flash("Khoj is not enabled - use search instead.")
        return redirect(url_for('blog.search'))

    if request.method == 'POST':
        # Query Khoj for posts based on the input data.
        error = None
        url_encoded_query = urllib.parse.quote(request.form.get("text"))

        try:
            results = requests.get(
                f'{khoj_url}/search?q={url_encoded_query}&t=panchayat&n=10&r=true'
            ).json()

            for result in results:
                post_id = int(result["entry"])
                queried_posts.append(vdb.posts.find(post_id))

        except TypeError:
            error = "Error while querying Khoj"

        if error is not None:
            flash(error)
    return render_template('blog/khoj.html',
                           posts=queried_posts,
                           root_url=ROOT_URL)


@BP.route('/feed', methods=['GET'])
@token_required
def feed():
    """
    Endpoint for RSS feed
    """
    blog_url = f'{ROOT_URL}{url_for("blog.index")}'
    feed_url = f'{ROOT_URL}{url_for("blog.feed")}'

    rss = AtomFeed('Panchayat RSS Feed', feed_url=feed_url, url=blog_url)

    vdb = get_db()
    # Sort post by created date
    posts = vdb.posts

    for post in posts.reverse_chrono():
        if (post.is_url() or post.parent is None) and not is_empty(post.title):
            title = post.title
            body = post.body
        elif not is_empty(post.body):
            title = post.body
            body = ""
        else:
            continue

        rss.add(title,
                body,
                content_type='text',
                author=post.author.username,
                url=f'{blog_url}#{post.post_id}',
                updated=post.created,
                published=post.created)

    return rss.get_response()


@BP.route('/posts/<int:post_id>', methods=('GET', ))
def post_page(post_id=None):
    """
    Endpoint when viewing single post in context of its TLP
    """
    if post_id is not None:
        vdb = get_db()
        post = vdb.posts.find(post_id)
        if post is None:
            flash(f'Post {post_id} does not exist')
        else:
            return render_template(
                'blog/tlp.html',
                post=post,
            )
    referrer = request.args.get('referrer', 'blog.index')
    return redirect(url_for(referrer))


@BP.route('/submit-url', methods=('GET', 'POST'))
@BP.route('/posts/<int:parent>/reply-url', methods=('GET', 'POST'))
@login_required
def reply_url(parent=None):
    """
    Endpoint when replying to post with a link
    parent is empty for top level post
    """
    if parent is not None:
        vdb = get_db()
        parent = vdb.posts.find(parent)
    return reply(parent=parent, is_url=True)


@BP.route('/submit-text', methods=('GET', 'POST'))
@BP.route('/posts/<int:parent>/reply-text', methods=('GET', 'POST'))
@login_required
def reply_text(parent=None):
    """
    Endpoint when replying to post with text
    parent is empty for top level post
    """
    if parent is not None:
        vdb = get_db()
        parent = vdb.posts.find(parent)
    return reply(parent=parent, is_url=False)


@BP.route('/submit', methods=('GET', 'POST'))
@mailauth_required
def submit(parent=None):  # pylint: disable=inconsistent-return-statements
    """
    Endpoint when submitting post via mailgun
    """
    vdb = get_db()
    parent = request.args.get('parent', None)
    parent = vdb.posts.find(int(parent)) if not is_empty(parent) else None
    title = request.form['subject']

    if 'stripped-text' in request.form:
        body = request.form['stripped-text']
    elif 'stripped-html' in request.form:
        body = request.form['stripped-html']
    else:
        body = ''

    if is_empty(title) and is_empty(body):
        return
    if not is_empty(title) and is_empty(body):
        # mail comments can just be in subject but db stores comments in body
        body, title = title.strip(), body.strip()
    else:
        title = title.strip()
        body = body.strip()

    is_url = body.startswith('http') and ' ' not in body

    # If submitted post is a URL without title
    if is_url and is_empty(title):
        suggested_title = ''

        # Try get title from website
        try:
            soup = BeautifulSoup(urlopen(body))
            suggested_title = soup.title.string
        except:  # pylint: disable=bare-except
            pass

        title = suggested_title if not is_empty(suggested_title) else title

    if is_url:
        PostCls = LinkPost
    else:
        PostCls = TextPost
    post = PostCls(author=g.user,
                   title=title,
                   body=body,
                   visibility=Visibility.Gram,
                   parent=parent)

    vdb.posts.insert(post)
    vdb.commit()
    broadcast_post(post)

    return jsonify(success=True)


@BP.route('/posts/<int:post_id>/update', methods=('GET', 'POST'))
@login_required
def update(post_id):
    """
    Endpoint when editing a post
    """
    vdb = get_db()
    try:
        post = vdb.posts.find(post_id)
    except RuntimeError:
        abort(404, "Post id {0} doesn't exist.".format(post_id))

    if post.author != g.user:
        abort(303)

    if request.method == 'POST':
        title = request.form.get('title', '')
        body = request.form['body']
        error = None

        if request.form['visibility'] == "Aham":
            visibility = Visibility.Aham
        elif request.form['visibility'] == "Gram":
            visibility = Visibility.Gram
        elif request.form['visibility'] == "Lok":
            visibility = Visibility.Lok
        else:
            error = 'Unknown visibility level'

        if error is not None:
            flash(error)
        else:
            post.title = title
            post.body = body
            prev_visibility = post.visibility
            try:
                post.visibility = visibility
            except RuntimeError as err:
                error = err
            if error is not None:
                flash(error)
            else:
                vdb.commit()
                if prev_visibility == Visibility.Aham and visibility != Visibility.Aham:
                    broadcast_post(post)

                referrer = request.args.get('referrer', 'blog.index')
                return redirect(url_for(referrer) + '#' + str(post_id))

    return render_template('blog/update.html', post=post)


@BP.route('/posts/<int:post_id>/delete', methods=('POST', ))
@login_required
def delete(post_id):
    """
    Endpoint when deleting a post
    Post deletion replaces the title and body with 'DELETED'
    Does not remove post from DB
    This is done to not corrupt downstream dependencies of this post
    for eg: child posts
    """
    vdb = get_db()
    try:
        post = vdb.posts.find(post_id)
    except RuntimeError:
        abort(404, "Post id {0} doesn't exist.".format(post_id))

    if post.author != g.user:
        abort(303)

    post.title = "DELETED"
    post.body = "DELETED"
    vdb.commit()

    referrer = request.args.get('referrer', 'blog.index')
    return redirect(url_for(referrer) + '#' + str(post_id))


@BP.route('/posts/<int:post_id>/upvote', methods=['GET'])
@login_required
def upvote(post_id):
    """
    Endpoint when upvoting a post
    """
    vdb = get_db()
    try:
        post = vdb.posts.find(post_id)
    except RuntimeError:
        abort(404, "Post id {0} doesn't exist.".format(post_id))
    if g.user in post.upvotes:
        post.nullvote(g.user)
    else:
        post.upvote(g.user)
    vdb.commit()

    referrer = request.args.get('referrer', 'blog.index')
    return redirect(url_for(referrer) + '#' + str(post_id))


@BP.route('/posts/<int:post_id>/downvote', methods=['GET'])
@login_required
def downvote(post_id):
    """
    Endpoint when downvoting a post
    """
    vdb = get_db()
    try:
        post = vdb.posts.find(post_id)
    except RuntimeError:
        abort(404, "Post id {0} doesn't exist.".format(post_id))
    if g.user in post.downvotes:
        post.nullvote(g.user)
    else:
        post.downvote(g.user)
    vdb.commit()
    referrer = request.args.get('referrer', 'blog.index')
    return redirect(url_for(referrer) + '#' + str(post_id))


@BP.route('/sitemap.xml', methods=['GET'])
def sitemap():
    """
    Endpoint for sitemap
    Help google crawler to index all and only TLPs
    """
    vdb = get_db()
    min_last_modified = current_app.config['SITEMAP_MIN_LAST_MODIFIED']
    template = render_template('blog/sitemap.xml',
                               posts=vdb.posts,
                               min_last_modified=min_last_modified)
    response = make_response(template)
    response.headers['Content-Type'] = 'application/xml'
    return response
