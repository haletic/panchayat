"""
Contains unit tests that test VDB in isolation
"""

import datetime
import pytest  # type: ignore
from panchayat.vdb import VDB, TextPost, LinkPost, User, Visibility

# pylint: disable=redefined-outer-name


@pytest.fixture
def empty_vdb():
    """
    Fixture returning an empty VDB
    """
    return VDB()


@pytest.fixture
def alpha_user():
    """
    Fixture returning a dummy user
    """
    return User('alpha', 'alpha123')


@pytest.fixture
def text_post(alpha_user):
    """
    Fixture returning a dummy text post
    """
    return TextPost(
        author=alpha_user,
        title='text_post_title',
        body='text_post_body',
        parent=None,
    )


@pytest.fixture
def link_post(text_post, alpha_user):
    """
    Fixture returning a dummy link post
    """
    return LinkPost(
        author=alpha_user,
        title='link_post_title',
        body='link_post_body',
        parent=text_post,
    )


@pytest.fixture
def alpha_user_vdb(empty_vdb, alpha_user):
    """
    Fixture returning a VDB containing dummy user
    """
    empty_vdb.users.append(alpha_user)
    return empty_vdb


@pytest.fixture
def text_post_vdb(alpha_user_vdb, text_post):
    """
    Fixture returning a VDB containing a dummy text post
    """
    alpha_user_vdb.posts.insert(text_post)
    return alpha_user_vdb


@pytest.fixture
def link_post_vdb(text_post_vdb, link_post):
    """
    Fixture returning a VDB containing a dummy link post
    """
    text_post_vdb.posts.insert(link_post)
    return text_post_vdb


def test_init_db(empty_vdb):
    """
    Test VDB constructor
    """
    assert len(empty_vdb.users) == 0
    assert len(empty_vdb.posts.all()) == 0


def test_new_user(alpha_user_vdb):
    """
    Test new user
    """
    assert alpha_user_vdb.users[0].username == 'alpha'
    assert alpha_user_vdb.users[0].password == 'alpha123'
    assert alpha_user_vdb.users[0].token is None
    assert alpha_user_vdb.users[0].email is None


def test_new_post(text_post_vdb):
    """
    Test new post
    """
    assert len(text_post_vdb.posts.all()) == 1
    assert len(text_post_vdb.posts.tlps) == 1
    post = text_post_vdb.posts.find(post_id=1)
    assert isinstance(post, TextPost)
    assert post.title == 'text_post_title'
    assert post.body == 'text_post_body'
    assert post.visibility == Visibility.Gram
    assert post.parent is None
    assert (datetime.datetime.now() - post.created) < \
        datetime.timedelta(seconds=5)


def test_reply(link_post_vdb, text_post, link_post):
    """
    Test reply post
    """
    assert len(link_post_vdb.posts.tlps) == 1
    assert len(link_post_vdb.posts.all()) == 2
    post = link_post_vdb.posts.find(post_id=2)
    assert isinstance(post, LinkPost)
    assert post.title == 'link_post_title'
    assert post.body == 'link_post_body'
    assert post.visibility == Visibility.Gram
    assert post.parent is text_post
    assert len(text_post.children) == 1
    assert text_post.children[0] is link_post


def test_delete_post(link_post):
    """
    Test delete post
    """
    link_post.delete()
    assert link_post.title == "DELETED"
    assert link_post.body == "DELETED"


@pytest.mark.skip(reason="Not implemented")
def test_vote(link_post_vdb):
    """
    Test voting
    """
    raise NotImplementedError
