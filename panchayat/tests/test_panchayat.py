"""
Contains all tests for panchayat functionality
"""

import datetime
import os
import re
import tempfile
from typing import Dict, List, Tuple

import pytest  # type: ignore
from werkzeug.security import check_password_hash

from panchayat import create_app
from panchayat.db import init_db
from panchayat.vdb import LinkPost, TextPost, User, Visibility

# pylint: disable=line-too-long, too-few-public-methods, redefined-outer-name, unused-argument
# pylint: disable=too-many-lines, fixme, too-many-arguments


class MailSimulator:
    """
    Class to simulate email functionality
    """
    sent_emails: List['Email'] = []

    class Email:
        """
        Class to represent a standard email object
        """
        def __init__(self,
                     from_address,
                     to_address,
                     subject,
                     body,
                     reply_to=None):
            # pylint: disable=too-many-arguments
            self.from_address = from_address
            self.to_address = to_address
            self.subject = subject
            self.body = body
            self.reply_to = reply_to

        def __str__(self):
            return f'To: {self.to_address}\n'\
                   f'Subject: {self.subject}\n'\
                   f'Body: {self.body}\n'\
                   f'Reply_to: {self.reply_to}'\

    class Mailgun:
        """
        Class to simulate Mailgun functionality
        """
        class MailgunRoute:
            """
            Class to represent a forward route in Mailgun
            """
            match_recipient = r'^panchayattest\+?([0-9]*)@haletic.com$'
            forward_address = r'/submit?parent=\1'

        @classmethod
        def webhook(cls, email) -> Tuple[str, Dict]:
            """
            Return webhook that would have been trigerred by Mailgun for an email
            """
            url = re.sub(cls.MailgunRoute.match_recipient,
                         cls.MailgunRoute.forward_address, email.to_address)
            form = {
                'From': f'<{email.from_address}>',
                'subject': email.subject,
                'stripped-html': email.body,
                'token': 'DEADBEEF',
                'timestamp': 'DEADBEEF',
                'signature': 'DEADBEEF',
            }

            return url, form

    @staticmethod
    def send_mail_monkeypatch(to_address: str, subject: str, body: str,
                              reply_to: str):
        """
        Monkey patch the send_mail function of panchayat to intercept function calls.
        Overide with this function which appends email to sent_emails
        """
        MailSimulator.sent_emails.append(
            MailSimulator.Email(
                from_address='panchayattest@haletic.com',
                to_address=to_address,
                subject=subject,
                body=body,
                reply_to=reply_to,
            ))

    @staticmethod
    def is_mailgun_monkeypatch(api_key, token, timestamp, signature):  #pylint: disable=unused-argument
        """
        Monkeypatch is_mailgun function of panchayat to bypass mailgun authentication.
        Panchayat only allows post via webhook after authenticating Mailgun with token.
        This allows us to trick Panchayat to submit webhook while testing without auth.
        """
        return True

    @classmethod
    def simulate_mail_receive(cls, email, client):
        """
        Simulate receiving an email which triggers the webhook
        """
        url, data = cls.Mailgun.webhook(email)
        client.post(url, data=data, follow_redirects=True)


@pytest.fixture
def app(monkeypatch):
    """
    Fixture that returns an instance of app
    """
    db_fd, db_path = tempfile.mkstemp()
    sitemap_min_last_modified = datetime.datetime.today() - datetime.timedelta(
        days=1)
    app = create_app({
        "TESTING": True,
        "DATABASE": db_path,
        "EXTERNAL_DOMAIN": "haletic.com",
        "SLUG": "panchayattest",
        "REGISTRATION_TOKEN": "test_token",
        "SITEMAP_MIN_LAST_MODIFIED": sitemap_min_last_modified
    })

    with app.app_context():
        init_db()
        # monkey patching panchayat send_mail function to intercept emails
        from panchayat.helpers import mail_utils  #pylint: disable=import-outside-toplevel
        from panchayat import auth  #pylint: disable=import-outside-toplevel
        monkeypatch.setattr(mail_utils, "send_mail",
                            MailSimulator.send_mail_monkeypatch)
        monkeypatch.setattr(auth, "is_mailgun",
                            MailSimulator.is_mailgun_monkeypatch)

    yield app

    os.close(db_fd)
    os.unlink(db_path)


@pytest.fixture
def vdb(app):
    """
    Fixture that returns the vdb of the app.
    Can only be called once in the test hierarchy.
    """
    return app.config['VDB']


@pytest.fixture
def client(app):
    """
    Fixtures that returns a test client for the app.
    Client has http get/post methods.
    """
    return app.test_client()


def test_index_before_log_in(client):
    """
    Test get panchayat index page
    """
    rv = client.get('/', follow_redirects=True)
    assert b'<title>Posts - Panchayat</title>' in rv.data


def test_register_page(client):
    """
    Test panchayat register page
    """
    rv = client.get('/auth/register')
    assert b'<h1>Register</h1>' in rv.data


def test_user_registration_errors(client):
    """
    Test invalid registration form submits
    """
    data = {
        "username": "",
        "password": "test123",
        "email": "",
        "token": "test_token"
    }
    rv = client.post('/auth/register', data=data, follow_redirects=True)
    assert b'Username is required.' in rv.data

    data = {
        "username": "test",
        "password": "",
        "email": "",
        "token": "test_token"
    }
    rv = client.post('/auth/register', data=data, follow_redirects=True)
    assert b'Password is required.' in rv.data

    data = {
        "username": "test",
        "password": "test123",
        "email": "",
        "token": "incorrect_token"
    }
    rv = client.post('/auth/register', data=data, follow_redirects=True)
    assert b'Invalid token.' in rv.data

    data = {
        "username": "test",
        "password": "test123",
        "email": "",
        "token": "test_token"
    }
    rv = client.post('/auth/register', data=data, follow_redirects=True)
    assert b'<h1>Log In</h1>' in rv.data

    data = {
        "username": "test",
        "password": "test123",
        "email": "",
        "token": "test_token"
    }
    rv = client.post('/auth/register', data=data, follow_redirects=True)
    assert b'User test is already registered' in rv.data


@pytest.fixture
def user_client(client):
    """
    Fixture that returns client containing dummy calvin and hobbes users
    """
    data = {
        "username": "calvin",
        "password": "test123",
        "email": "calvin@calvinism.org",
        "token": "test_token"
    }
    _ = client.post('/auth/register', data=data, follow_redirects=True)

    data = {
        "username": "hobbes",
        "password": "test123",
        "email": "hobbes@leviathan.org",
        "token": "test_token"
    }
    _ = client.post('/auth/register', data=data, follow_redirects=True)

    return client


def test_user_registration_vdb(user_client, vdb):
    """
    Test user registration reflecting in vdb
    """
    assert len(vdb.users) == 2
    test_user = vdb.users.find('calvin')
    assert test_user.username == 'calvin'
    assert check_password_hash(test_user.password, 'test123')
    assert test_user.gravatar_url(
    ) == "https://www.gravatar.com/avatar/89415f139f92a522517473ec6b6e0c20?size=200&default=identicon"

    test2_user = vdb.users.find('hobbes')
    assert test2_user.username == 'hobbes'
    assert check_password_hash(test_user.password, 'test123')
    assert test2_user.gravatar_url(
    ) == "https://www.gravatar.com/avatar/cde9db4e0900178168a457d7b9997df3?size=200&default=identicon"


def test_log_in(user_client):
    """
    Test invalid and valid login attempts
    """
    data = {"username": "", "password": "test123"}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'Invalid username' in rv.data

    data = {"username": "calvin", "password": ""}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'Invalid password' in rv.data

    data = {"username": "testx", "password": "test123"}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'Incorrect username' in rv.data

    data = {"username": "calvin", "password": "test123x"}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'Incorrect password' in rv.data

    data = {"username": "calvin", "password": "test123"}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'<h1>Posts</h1>' in rv.data


@pytest.fixture
def logged_in_client_calvin(user_client):
    """
    Fixture to return client containing a logged in user
    """
    data = {"username": "calvin", "password": "test123"}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'title=calvin' in rv.data
    return user_client


@pytest.fixture
def logged_in_client_hobbes(user_client):
    """
    Fixture to return client containing a logged in user
    """
    data = {"username": "hobbes", "password": "test123"}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'title=hobbes' in rv.data
    return user_client


def test_new_post_page(logged_in_client_calvin):
    """
    Test get new post page
    """
    rv = logged_in_client_calvin.get('/submit-text', follow_redirects=True)
    assert b'<label for="title">Title</label' in rv.data
    assert b'<input name="title" id="title" value="">' in rv.data
    assert b'<label for="body">Body</label>' in rv.data
    assert b'<textarea name="body" id="body"></textarea>' in rv.data

    rv = logged_in_client_calvin.get('/submit-url', follow_redirects=True)
    assert b'<label for="title">Title</label' in rv.data
    assert b'<input name="title" id="title" value="">' in rv.data
    assert b'<label for="body">Link URL</label>' in rv.data
    assert b'<input name="body" id="body" value="">' in rv.data


@pytest.fixture
def link_post_client(logged_in_client_calvin):
    """
    Fixture to return client containing dummy link post
    """
    data = {
        "title": "alpha_post_title",
        "body": "alpha_post_url",
        "visibility": "Gram"
    }
    _ = logged_in_client_calvin.post('/submit-url',
                                     data=data,
                                     follow_redirects=True)
    return logged_in_client_calvin


@pytest.fixture
def text_post_client(logged_in_client_calvin):
    """
    Fixture to return client with dummy text client
    """
    data = {
        "title": "alpha_post_title",
        "body": "alpha_post_body",
        "visibility": "Lok"
    }
    _ = logged_in_client_calvin.post('/submit-text',
                                     data=data,
                                     follow_redirects=True)
    return logged_in_client_calvin


def test_submit_url(link_post_client, vdb):
    """
    Test submitting link post reflecting in VDB
    """
    assert len(vdb.posts.all()) == 1
    post = vdb.posts.find(1)
    assert isinstance(post, LinkPost)
    assert post.title == "alpha_post_title"
    assert post.body == "alpha_post_url"
    assert post.author.username == "calvin"


def test_submit_text(text_post_client, vdb):
    """
    Test submitting text post reflecting in VDB
    """
    assert len(vdb.posts.all()) == 1
    post = vdb.posts.find(1)
    assert isinstance(post, TextPost)
    assert post.title == "alpha_post_title"
    assert post.body == "alpha_post_body"
    assert post.author.username == "calvin"


def test_edit_text_post_page(text_post_client):
    """
    Test http get for editing a page
    """
    rv = text_post_client.get('/posts/1/update?referrer=blog.index',
                              follow_redirects=True)
    assert b'<h1>Edit</h1>' in rv.data
    assert b'<input name="title" id="title"\n           value="alpha_post_title">' in rv.data
    assert b'<textarea name="body" id="body">alpha_post_body</textarea>' in rv.data


def test_edit_link_post_page(link_post_client):
    """
    Test http get for editing a page
    """
    rv = link_post_client.get('/posts/1/update?referrer=blog.index',
                              follow_redirects=True)
    assert b'<h1>Edit</h1>' in rv.data
    assert b'<input name="title" id="title"\n           value="alpha_post_title">' in rv.data
    assert b'<textarea name="body" id="body">alpha_post_url</textarea>' in rv.data


@pytest.fixture
def edit_post_client(text_post_client):
    """
    Fixture returning client with an edited post
    """
    data = {
        "title": "edited_post_title",
        "body": "edited_post_body",
        "visibility": "Gram"
    }
    _ = text_post_client.post('posts/1/update',
                              data=data,
                              follow_redirects=True)
    return text_post_client


@pytest.fixture
def deleted_post_client(text_post_client):
    """
    Fixture returning client with a deleted post
    """
    _ = text_post_client.post('posts/1/delete', follow_redirects=True)
    return text_post_client


def test_edit_post(edit_post_client, vdb):
    """
    Text post edit reflected in VDB
    """
    post = vdb.posts.find(1)
    assert post.title == "edited_post_title"
    assert post.body == "edited_post_body"


def test_delete_post(deleted_post_client, vdb):
    """
    Test post deletion reflected in VDB
    """
    post = vdb.posts.find(1)
    assert post.title == "DELETED"
    assert post.body == "DELETED"


@pytest.fixture
def reply_text_post_client(text_post_client):
    """
    Fixture returning client with a text reply comment
    """
    data = {
        "body": "reply_post_body",
        "visibility": "Lok",
        "save_button": "Save"
    }
    _ = text_post_client.post('/posts/1/reply-text',
                              data=data,
                              follow_redirects=True)
    return text_post_client


@pytest.mark.parametrize("page", ['/posts/1', '/posts/2'])
def test_post_page(reply_text_post_client, page):
    """
    Test dedicated page for individual posts
    """
    rv = reply_text_post_client.get(page, follow_redirects=True)
    assert b'<p class="body"><p>alpha_post_body</p>\n</p>' in rv.data
    assert b'<p class="body"><p>reply_post_body</p>\n</p>' in rv.data


def test_invalid_post_page(reply_text_post_client):
    """
    Test invalid requests for dedicated post pages
    """
    rv = reply_text_post_client.get('/posts/3', follow_redirects=True)
    assert b'Post 3 does not exist' in rv.data


@pytest.fixture
def reply_text_post_different_user_client(text_post_client,
                                          logged_in_client_hobbes):
    """
    Fixture returning client with a text reply comment
    """
    data = {
        "body": "reply_post_body",
        "visibility": "Gram",
        "save_button": "Save"
    }
    _ = logged_in_client_hobbes.post('/posts/1/reply-text',
                                     data=data,
                                     follow_redirects=True)
    return logged_in_client_hobbes


def test_reply_post(reply_text_post_client, vdb):
    """
    Text reply post reflecting in VDB
    """
    assert len(vdb.posts.all()) == 2
    parent_post = vdb.posts.find(1)
    reply_post = vdb.posts.find(2)
    assert parent_post.children[0] == reply_post
    assert isinstance(reply_post, TextPost)
    assert reply_post.parent == parent_post
    assert parent_post.author == vdb.users.find('calvin')
    assert reply_post.author == vdb.users.find('calvin')


def test_reply_post_different_user(reply_text_post_different_user_client, vdb):
    """
    Test replying to another user's post
    """
    assert len(vdb.posts.all()) == 2
    parent_post = vdb.posts.find(1)
    reply_post = vdb.posts.find(2)
    assert parent_post.author == vdb.users.find('calvin')
    assert reply_post.author == vdb.users.find('hobbes')


@pytest.fixture
def upvote_client(text_post_client):
    """
    Fixture returning client with an upvoted post
    """
    _ = text_post_client.get('/posts/1/upvote', follow_redirects=True)
    return text_post_client


@pytest.fixture
def downvote_client(text_post_client):
    """
    Fixture returning client with a downvoted client
    """
    _ = text_post_client.get('/posts/1/downvote', follow_redirects=True)
    return text_post_client


@pytest.fixture
def unvote_client(text_post_client):
    """
    Fixture returning client with multiple upvote/downvote actions
    """
    _ = text_post_client.get('/posts/1/upvote', follow_redirects=True)
    _ = text_post_client.get('/posts/1/upvote', follow_redirects=True)
    _ = text_post_client.get('/posts/1/downvote', follow_redirects=True)
    _ = text_post_client.get('/posts/1/downvote', follow_redirects=True)
    return text_post_client


def test_vote(upvote_client, vdb):
    """
    Test upvote.
    """
    user = vdb.users.find("calvin")
    post = vdb.posts.find(1)
    assert len(post.upvotes) == 1
    assert len(post.downvotes) == 0
    assert user in post.upvotes


def test_downvote(downvote_client, vdb):
    """
    Test downvoting
    """
    user = vdb.users.find("calvin")
    post = vdb.posts.find(1)
    assert len(post.upvotes) == 0
    assert len(post.downvotes) == 1
    assert user in post.downvotes


def test_unvote(unvote_client, vdb):
    """
    Test idempotence of voting
    """
    post = vdb.posts.find(1)
    assert len(post.upvotes) == 0
    assert len(post.downvotes) == 0


def test_user_page(logged_in_client_calvin):
    """
    Test get user page
    """
    rv = logged_in_client_calvin.get('/user/', follow_redirects=True)
    assert b'<h3>User Settings</h3>' in rv.data


def test_sitemap(reply_text_post_client):
    """
    Test sitemap
    """
    rv = reply_text_post_client.get('/sitemap.xml', follow_redirects=True)
    assert b'posts/1' in rv.data
    assert b'posts/2' not in rv.data


def test_user_stats(upvote_client, vdb):
    """
    Test user stats
    """
    user = vdb.users.find("calvin")
    assert vdb.posts.tlp_count(user) == 1
    assert vdb.posts.comment_count(user) == 0
    assert vdb.posts.upvote_count(user) == 1
    assert vdb.posts.downvote_count(user) == 0


@pytest.fixture
def generated_token_client(logged_in_client_calvin):
    """
    Fixture returning client with a generated token for dummy user
    """
    _ = logged_in_client_calvin.post('/user/token?action=generate',
                                     follow_redirects=True)
    return logged_in_client_calvin


@pytest.fixture
def destroyed_token_client(generated_token_client):
    """
    Fixture returning client after destroying token for dummy user
    """
    _ = generated_token_client.post('/user/token?action=destroy',
                                    follow_redirects=True)
    return generated_token_client


def test_generate_token(generated_token_client, vdb):
    """
    Test token generation for user
    """
    user = vdb.users.find("calvin")
    assert user.token is not None


def test_destroy_token(destroyed_token_client, vdb):
    """
    Test token destruction for user
    """
    user = vdb.users.find("calvin")
    assert user.token == ""


def test_activity_page(logged_in_client_calvin):
    """
    Test get activity page
    """
    rv = logged_in_client_calvin.get('/activity', follow_redirects=True)
    assert b'<h1>Activity</h1>' in rv.data


def test_rss(text_post_client):
    """
    Text get RSS feed
    """
    rv = text_post_client.get('/feed', follow_redirects=True)
    assert b'<title type="text">Panchayat RSS Feed</title>' in rv.data


@pytest.fixture
def enable_test_email(user_client, vdb):
    """
    Update vdb to add mailgun user, enable email for calvin
    """
    test_user = vdb.users.find('calvin')
    test_user.email = "test_user@email_provider.com"
    test_user.email_updates = True

    vdb.users.append(
        User(
            username='mailgun',
            password='',
            token='mailgun_api_key',
        ))
    vdb.commit()

    yield

    #flush sent emails after test finishes
    MailSimulator.sent_emails = []


@pytest.fixture
def text_email_receive(user_client, enable_test_email):
    """
    Fixture that simulates receiving dummy new text post email
    Fixture invokes Mailgun simulator and triggers the appropriate webhook
    """
    email = MailSimulator.Email(from_address='test_user@email_provider.com',
                                to_address="panchayattest@haletic.com",
                                subject="alpha_post_title",
                                body="alpha_post_body")
    MailSimulator.simulate_mail_receive(email, user_client)


class TestEmail:
    """
    Class to encapsulate all email related tests
    """
    @staticmethod
    def test_link_email_send(enable_test_email, link_post_client):
        """
        Test posting a link sends right email
        """
        assert len(MailSimulator.sent_emails) == 1
        sent_email = MailSimulator.sent_emails[-1]
        assert sent_email.to_address == "test_user@email_provider.com"
        assert sent_email.subject == "alpha_post_title"
        assert '<a href="alpha_post_url">alpha_post_title</a>' in sent_email.body
        assert '<a class="action" href="mailto:panchayattest+1@haletic.com?subject=alpha_post_title">Reply</a>' in sent_email.body
        assert '<a class="action" href="mailto:panchayattest@haletic.com">New Post</a>' in sent_email.body
        assert sent_email.reply_to == 'panchayattest+1@haletic.com'

    @staticmethod
    def test_text_email_send(enable_test_email, text_post_client):
        """
        Test posting text sends right email
        """
        assert len(MailSimulator.sent_emails) == 1
        sent_email = MailSimulator.sent_emails[-1]
        assert sent_email.to_address == "test_user@email_provider.com"
        assert sent_email.subject == "alpha_post_title"
        assert '<h1>\n            \n                \n                    alpha_post_title\n                \n            \n        </h1>' in sent_email.body
        assert '<p class="body" style="white-space: pre-line; word-wrap: break-word;">alpha_post_body</p>' in sent_email.body
        assert '<a class="action" href="mailto:panchayattest+1@haletic.com?subject=alpha_post_title">Reply</a>' in sent_email.body
        assert '<a class="action" href="mailto:panchayattest@haletic.com">New Post</a>' in sent_email.body
        assert sent_email.reply_to == 'panchayattest+1@haletic.com'

    @staticmethod
    def test_text_comment_email_send(enable_test_email,
                                     reply_text_post_client):
        """
        Test replying to a post sends right email
        """
        assert len(MailSimulator.sent_emails) == 2
        sent_email = MailSimulator.sent_emails[-1]
        assert sent_email.to_address == "test_user@email_provider.com"

        #subject is the tlp post subject to enable threading emails
        assert sent_email.subject == "alpha_post_title"
        assert '<p class="body" style="white-space: pre-line; word-wrap: break-word;">reply_post_body</p>' in sent_email.body
        assert '<a class="action" href="mailto:panchayattest+2@haletic.com?subject=alpha_post_title">Reply</a>' in sent_email.body
        assert '<a class="action" href="mailto:panchayattest@haletic.com">New Post</a>' in sent_email.body
        assert sent_email.reply_to == 'panchayattest+2@haletic.com'

    @staticmethod
    def test_text_email_receive(text_email_receive, app, vdb):
        """
        Test receiving new text post email reflected in VDB
        """
        assert len(vdb.posts.all()) == 1
        post = vdb.posts.find(1)
        assert isinstance(post, TextPost)
        assert post.title == "alpha_post_title"
        assert post.body == "alpha_post_body"

    @staticmethod
    @pytest.fixture
    def link_email_receive(user_client, enable_test_email):
        """
        Fixture that simulates receiving dummy new link post email
        Fixture invokes Mailgun simulator and triggers the appropriate webhook
        """
        email = MailSimulator.Email(
            from_address='test_user@email_provider.com',
            to_address="panchayattest@haletic.com",
            subject="alpha_post_title",
            body="http://www.alpha_post_url.com")
        MailSimulator.simulate_mail_receive(email, user_client)

    @staticmethod
    def test_link_email_receive(link_email_receive, app, vdb):
        """
        Test receiving new link post email reflected in VDB
        """
        assert len(vdb.posts.all()) == 1
        post = vdb.posts.find(1)
        assert isinstance(post, LinkPost)
        assert post.title == "alpha_post_title"
        assert post.body == "http://www.alpha_post_url.com"

    @staticmethod
    @pytest.fixture
    def text_email_comment(link_post_client, enable_test_email):
        """
        Fixture that simulates receiving a reply email
        Fixture invokes Mailgun simulator and triggers the appropriate webhook
        """
        email = MailSimulator.Email(
            from_address='test_user@email_provider.com',
            to_address="panchayattest+1@haletic.com",
            subject="alpha_comment_title",
            body="alpha_comment_body")
        MailSimulator.simulate_mail_receive(email, link_post_client)

    @staticmethod
    def test_text_email_comment(text_email_comment, app, vdb):
        """
        Test receiving reply email reflected in VDB
        """
        assert len(vdb.posts.all()) == 2
        parent = vdb.posts.find(1)
        comment = vdb.posts.find(2)
        assert comment.parent is parent
        assert isinstance(comment, TextPost)
        assert comment.title == "alpha_comment_title"
        assert comment.body == "alpha_comment_body"


class TestVisibility:
    """
    Class to encapsulate tests related to visibility levels of posts
    """
    @staticmethod
    @pytest.fixture
    def aham_post_client(logged_in_client_calvin):
        """
        Fixture returning client with a aham post
        """
        data = {
            "title": "aham_post_title",
            "body": "aham_post_body",
            "visibility": "Aham"
        }
        _ = logged_in_client_calvin.post('/submit-text',
                                         data=data,
                                         follow_redirects=True)
        return logged_in_client_calvin

    @staticmethod
    @pytest.fixture
    def gram_post_client(logged_in_client_calvin):
        """
        Fixture returning client with a aham post
        """
        data = {
            "title": "gram_post_title",
            "body": "gram_post_body",
            "visibility": "Gram"
        }
        _ = logged_in_client_calvin.post('/submit-text',
                                         data=data,
                                         follow_redirects=True)
        return logged_in_client_calvin

    @staticmethod
    @pytest.fixture
    def reply_gram_post_client(gram_post_client):
        """
        Fixture returning client with a text reply comment
        """
        data = {
            "body": "reply_post_body",
            "visibility": "Gram",
            "save_button": "Save"
        }
        _ = gram_post_client.post('/posts/1/reply-text',
                                  data=data,
                                  follow_redirects=True)
        return gram_post_client

    @staticmethod
    @pytest.fixture
    def grandreply_gram_post_client(reply_gram_post_client):
        """
        Fixture returning client with a text reply comment
        """
        data = {
            "body": "grandreply_post_body",
            "visibility": "Gram",
            "save_button": "Save"
        }
        _ = reply_gram_post_client.post('/posts/2/reply-text',
                                        data=data,
                                        follow_redirects=True)
        return reply_gram_post_client

    @staticmethod
    @pytest.fixture
    def lok_post_client(logged_in_client_calvin):
        """
        Fixture returning client with a Lok post
        """
        data = {
            "title": "lok_post_title",
            "body": "lok_post_body",
            "visibility": "Lok"
        }
        _ = logged_in_client_calvin.post('/submit-text',
                                         data=data,
                                         follow_redirects=True)
        return logged_in_client_calvin

    @staticmethod
    def test_new_post_page(logged_in_client_calvin):
        """
        Test visibility options in both url and link new post page
        "Select" must be selected by default
        """
        rv = logged_in_client_calvin.get('/submit-text', follow_redirects=True)
        assert b'<label for="visibility">Visibility</label>\n    <select name="visibility" id="visibility">\n        <option value="Select" selected>Select</option>\n        <option value="Aham">Aham - Only visible to you</option>\n        <option value="Gram">Gram - Visible to all logged in users</option>\n        <option value="Lok">Lok - Visible without logging in</option>\n    </select>' in rv.data

        rv = logged_in_client_calvin.get('/submit-url', follow_redirects=True)
        assert b'<label for="visibility">Visibility</label>\n    <select name="visibility" id="visibility">\n        <option value="Select" selected>Select</option>\n        <option value="Aham">Aham - Only visible to you</option>\n        <option value="Gram">Gram - Visible to all logged in users</option>\n        <option value="Lok">Lok - Visible without logging in</option>\n    </select>' in rv.data

    @staticmethod
    def test_lok_post(lok_post_client, vdb):
        """
        Test post with lok visibility
        """
        assert len(vdb.posts.all()) == 1
        post = vdb.posts.find(1)
        assert isinstance(post, TextPost)
        assert post.visibility == Visibility.Lok

    @staticmethod
    def test_gram_post(gram_post_client, vdb):
        """
        Tests post with gram visibility
        text_post_client fixture can be used since default is gram visibility
        """
        assert len(vdb.posts.all()) == 1
        post = vdb.posts.find(1)
        assert isinstance(post, TextPost)
        assert post.visibility == Visibility.Gram

    @staticmethod
    def test_aham_post(aham_post_client, vdb):
        """
        Test post with aham visibility
        """
        assert len(vdb.posts.all()) == 1
        post = vdb.posts.find(1)
        assert isinstance(post, TextPost)
        assert post.visibility == Visibility.Aham

    @staticmethod
    def test_pending_visibility(grandreply_gram_post_client, vdb):
        """
        Test pending visibility and visibility requested
        """
        data = {
            "title": "grandreply_post_title",
            "body": "grandreply_post_body",
            "visibility": "Lok"
        }
        _ = grandreply_gram_post_client.post('/posts/3/update',
                                             data=data,
                                             follow_redirects=True)
        post = vdb.posts.find(1)
        reply = vdb.posts.find(2)
        grandreply = vdb.posts.find(3)

        # grandchild's target visibility must be Lok
        assert grandreply.target_visibility == Visibility.Lok

        # but it shouldn't actually be Lok since parents are not lok yet
        assert grandreply.visibility == Visibility.Gram

        # it should show lok pending though
        assert grandreply.visibility_detail_string() == "(Lok pending)"

        # all ancestors must have lok request
        assert post.visibility_detail_string() == "(Lok requested)"
        assert reply.visibility_detail_string() == "(Lok requested)"

        # now make reply lok
        data = {
            "title": "reply_post_title",
            "body": "reply_post_body",
            "visibility": "Lok"
        }
        _ = grandreply_gram_post_client.post('/posts/2/update',
                                             data=data,
                                             follow_redirects=True)

        # grandreply should still not be lok
        assert grandreply.visibility == Visibility.Gram

        # reply must have gone from lok requested to lok pending
        assert reply.visibility_detail_string() == "(Lok pending)"

        # finally make post lok as well
        data = {
            "title": "post_title",
            "body": "post_body",
            "visibility": "Lok"
        }
        _ = grandreply_gram_post_client.post('/posts/1/update',
                                             data=data,
                                             follow_redirects=True)

        # now all posts must have become lok
        assert grandreply.visibility == Visibility.Lok
        assert reply.visibility == Visibility.Lok
        assert post.visibility == Visibility.Lok

    @staticmethod
    def test_aham_with_others_child(reply_text_post_different_user_client):
        """
        Making parent aham with child owned by another user should throw error
        """

        # login as calvin
        data = {"username": "calvin", "password": "test123"}
        rv = reply_text_post_different_user_client.post('/auth/',
                                                        data=data,
                                                        follow_redirects=True)
        assert b'title=calvin' in rv.data

        # try to make parent post aham
        data = {
            "title": "alpha_post_title",
            "body": "alpha_post_body",
            "visibility": "Aham"
        }
        rv = reply_text_post_different_user_client.post('posts/1/update',
                                                        data=data,
                                                        follow_redirects=True)
        assert b'<div class="flash">Cannot make post Aham if there are children owned by others</div>' in rv.data

    @staticmethod
    def test_aham_with_different_parent(reply_text_post_different_user_client,
                                        vdb):
        """
        Test making child post aham with parent owned by other user
        """
        data = {
            "title": "aham_post_title",
            "body": "aham_post_body",
            "visibility": "Aham"
        }
        _ = reply_text_post_different_user_client.post('posts/2/update',
                                                       data=data,
                                                       follow_redirects=True)
        assert vdb.posts.find(2).visibility == Visibility.Aham

    @staticmethod
    def test_reply_other_user_aham(aham_post_client, logged_in_client_hobbes,
                                   vdb):
        """
        Hobbes should not be allowed to reply to Calvin's Aham post
        """
        data = {
            "body": "reply_post_body",
            "visibility": "Aham",
            "save_button": "Save"
        }
        rv = logged_in_client_hobbes.post('/posts/1/reply-text',
                                          data=data,
                                          follow_redirects=True)
        assert b'<div class="flash">Cannot reply to someone else&#39;s aham post</div>' in rv.data

    @staticmethod
    def test_tlp_visibility_caps_child_visibility(reply_gram_post_client, vdb):
        """
        Reducing the visibility of parent must reduce visibility of all descendants
        """
        data = {
            "title": "aham_post_title",
            "body": "aham_post_body",
            "visibility": "Aham"
        }
        _ = reply_gram_post_client.post('posts/1/update',
                                        data=data,
                                        follow_redirects=True)
        assert len(vdb.posts.all()) == 2
        parent_post = vdb.posts.find(1)
        reply_post = vdb.posts.find(2)
        assert parent_post.visibility == Visibility.Aham
        assert reply_post.visibility == Visibility.Aham

    @staticmethod
    @pytest.mark.parametrize("page", ['/', '/activity'])
    def test_page_visibility(page, user_client, aham_post_client,
                             gram_post_client, lok_post_client, vdb):
        """
        Test visibility levels of posts on homepage and activity page
        """

        # already logged in as calvin
        # calvin should be able to see all three posts
        rv = user_client.get(page, follow_redirects=True)

        assert b'<p class="body"><p>aham_post_body</p>\n</p>' in rv.data
        assert b'<p class="body"><p>gram_post_body</p>\n</p>' in rv.data
        assert b'<p class="body"><p>lok_post_body</p>\n</p>' in rv.data

        # login as hobbes
        # hobbes must be able to see the lok and gram post but not calvin's aham post
        data = {"username": "hobbes", "password": "test123"}
        rv = user_client.post('/auth/', data=data, follow_redirects=True)
        assert b'title=hobbes' in rv.data
        rv = user_client.get(page, follow_redirects=True)
        assert b'<p class="body"><p>aham_post_body</p>\n</p>' not in rv.data
        assert b'<p class="body"><p>gram_post_body</p>\n</p>' in rv.data
        assert b'<p class="body"><p>lok_post_body</p>\n</p>' in rv.data

        # logout
        # public user must be able to see lok post but not gram or aham
        _ = user_client.get('/auth/logout', data=data, follow_redirects=True)
        rv = user_client.get(page, follow_redirects=True)
        assert b'<p class="body"><p>aham_post_body</p>\n</p>' not in rv.data
        assert b'<p class="body"><p>gram_post_body</p>\n</p>' not in rv.data
        assert b'<p class="body"><p>lok_post_body</p>\n</p>' in rv.data

    @staticmethod
    def test_new_post_from_email(text_email_receive, vdb):
        """
        Creating new post via email should default to gram visibility
        """
        assert len(vdb.posts.all()) == 1
        post = vdb.posts.find(1)
        assert post.visibility == Visibility.Gram

    @staticmethod
    def test_email_visibility(
        enable_test_email,
        aham_post_client,
        gram_post_client,
        lok_post_client,
    ):
        """
        Creating gram and lok post must broadcast email
        Creating aham post must not send out email
        """
        assert len(MailSimulator.sent_emails) == 2
        assert MailSimulator.sent_emails[0].subject == "gram_post_title"
        assert MailSimulator.sent_emails[1].subject == "lok_post_title"

    @staticmethod
    def test_change_aham_email(enable_test_email, aham_post_client):
        """
        Changing aham post to non aham should send email
        """

        # update aham post to gram
        data = {
            "title": "gram_post_title",
            "body": "gram_post_body",
            "visibility": "Gram"
        }
        _ = aham_post_client.post('posts/1/update',
                                  data=data,
                                  follow_redirects=True)
        assert len(MailSimulator.sent_emails) == 1
        assert MailSimulator.sent_emails[-1].subject == "gram_post_title"

        MailSimulator.sent_emails = []

        # update gram post to lok. no email sent
        data = {
            "title": "lok_post_title",
            "body": "lok_post_body",
            "visibility": "Lok"
        }
        _ = aham_post_client.post('posts/1/update',
                                  data=data,
                                  follow_redirects=True)
        assert len(MailSimulator.sent_emails) == 0
