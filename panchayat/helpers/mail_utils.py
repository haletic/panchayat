"""
Contains mail related functions
"""

import requests
from flask import current_app, render_template

from panchayat.db import get_db
from panchayat.vdb import Post

ROOT_URL = current_app.config['ROOT_URL']
ROOT_EMAIL = current_app.config['ROOT_EMAIL']
SLUG = current_app.config['SLUG']
EXTERNAL_DOMAIN = current_app.config['EXTERNAL_DOMAIN']

# pylint: disable=line-too-long


def send_mail(to_address: str, subject: str, body: str, reply_to: str = None):
    """
    Send single email
    Posts to Mailgun API endpoint to trigger sending email
    """
    print(
        f'To: {to_address}\nSubject: {subject}\n'
        f'Body: {body}\nvia {current_app.config["MAILGUN_API"]}, {current_app.config["MAILGUN_KEY"]}'
    )
    data = {
        "from": f"{current_app.config['ROOT_EMAIL']}",
        "to": to_address,
        "subject": subject,
        "html": body,
    }
    if reply_to:
        data["h:Reply-To"] = reply_to

    requests.post(current_app.config['MAILGUN_API'],
                  auth=("api", current_app.config['MAILGUN_KEY']),
                  data=data)


def broadcast_post(post: Post):
    """
    Broadcast post.
    Creates and sends email for every subscribing user.
    """
    vdb = get_db()
    for user in vdb.users:
        if user.email_updates is not True:
            continue
        subject = post.tlp.title
        body = render_template(
            'email/email.html',
            new_post=post,
            root_url=ROOT_URL,
            slug=SLUG,
            root_email=ROOT_EMAIL,
            external_domain=EXTERNAL_DOMAIN,
        )
        reply_to = f"{SLUG}+{post.post_id}@{EXTERNAL_DOMAIN}"
        send_mail(to_address=user.email,
                  subject=subject,
                  body=body,
                  reply_to=reply_to)
