"""
Contains Auth related helper functions
"""

import secrets
import hmac
import hashlib

from panchayat.helpers.string_utils import is_empty

from panchayat.db import get_db


def token_handler(action, user):
    """
    Token helper function
    """
    if action == 'generate':
        token = secrets.token_urlsafe(20)
        vdb = get_db()
        user.token = token
        vdb.commit()

    elif action == 'destroy':
        token = ''
        vdb = get_db()
        user.token = token
        vdb.commit()

    elif action == 'get':
        token = user.token
    else:
        token = ''

    return token


def is_valid_token(request):
    """
    Returns true if token is valid
    """
    token = request.args.get('token', None)
    if not is_empty(token):
        vdb = get_db()
        if [user for user in vdb.users if user.token == token]:
            return True
    return False


def is_mailgun(api_key, token, timestamp, signature):
    """
    Returns true if mailgun was authenticated
    """
    # See https://documentation.mailgun.com/en/latest/user_manual.html#securing-webhooks for details
    hmac_digest = hmac.new(key=api_key.encode('utf-8'),
                           msg='{}{}'.format(timestamp, token).encode('utf-8'),
                           digestmod=hashlib.sha256).hexdigest()
    return hmac.compare_digest(signature.encode('utf-8'),
                               hmac_digest.encode('utf-8'))
