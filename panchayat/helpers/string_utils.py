"""
Contains string helper functions
"""


def is_empty(text):
    """
    Returns true if a string is empty
    We consider 'DELETED' and 'N/A' as empty in addition to empty string
    """
    restricted_words = set(['DELETED', 'N/A', ''])
    return text is None or text.strip() in restricted_words
