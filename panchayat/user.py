"""
All user related endpoints for panchayat
"""

from flask import (Blueprint, flash, g, redirect, render_template, request,
                   url_for)
from werkzeug.security import check_password_hash, generate_password_hash

from panchayat.db import get_db
from panchayat.auth import login_required, load_logged_in_user
from panchayat.helpers.string_utils import is_empty
from panchayat.helpers.auth_utils import token_handler

BP = Blueprint("user", __name__, url_prefix='/user')


@BP.route('/', methods=['GET', 'POST'])
@login_required
def index():
    """
    User main page endpoint
    """
    vdb = get_db()
    if request.method == 'POST':
        # extract fields from form
        username = request.form['username']
        email = request.form['email']
        entered_password = request.form['entered-password']
        current_password = g.user.password
        new_password = request.form['new-password']
        new_password_2 = request.form['new-password-2']

        # form does not submit checkbox if unchecked
        # https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/checkbox
        email_updates = request.form[
            'email-updates'] if 'email-updates' in request.form else "off"
        # validate POST fields for user update
        if not check_password_hash(current_password, entered_password):
            flash('Incorrect password')
        elif new_password != new_password_2:
            flash('New Passwords Mismatch')
        elif is_empty(username):
            flash('Empty username')
        elif email_updates == "on" and email == "":
            flash('Email address required for email updates')
        else:
            # update current user's username, email and/or password
            password = entered_password if is_empty(
                new_password) else new_password
            g.user.username = username
            g.user.password = generate_password_hash(password)
            g.user.email = email
            g.user.email_updates = (email_updates == "on")
            flash('User settings updated')
            vdb.commit()

            load_logged_in_user(force=True)

    return render_template('blog/user.html', posts=vdb.posts, settings=True)


@BP.route('/<string:username>', methods=['GET'])
@login_required
def user(username):
    """
    User end page for specific user
    """
    vdb = get_db()
    if username == g.user.username:
        return redirect(url_for('user.index'))
    return render_template('blog/user.html', posts=vdb.posts, settings=False)


@BP.route('/delete', methods=['POST'])
@login_required
def delete():
    """
    Delete a user from panchayat
    """
    vdb = get_db()
    g.user.username = "DELETED"
    g.user.password = "DELETED"
    vdb.commit()

    g.user = None
    return redirect(url_for('auth.login'))


@BP.route('/token', methods=['GET', 'POST'])
@login_required
def token():
    """
    Token endpoint for user
    """
    action = request.args.get('action', None)
    _ = token_handler(action, user=g.user)
    referrer = request.args.get('referrer', 'user.index')
    return redirect(url_for(referrer))
