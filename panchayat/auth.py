"""
Contains all authentication end points
"""

import functools

from flask import (Blueprint, flash, g, redirect, render_template, request,
                   session, url_for, abort, current_app)
from werkzeug.security import check_password_hash, generate_password_hash

from panchayat.db import get_db
from panchayat.helpers.string_utils import is_empty
from panchayat.helpers.auth_utils import is_valid_token, is_mailgun, token_handler
from panchayat.helpers.mail_utils import send_mail
from panchayat.vdb import User

BP = Blueprint("auth", __name__, url_prefix='/auth')


@BP.route('/register', methods=('GET', 'POST'))
def register():
    """
    Register page endpoint
    """
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        token = request.form['token']
        vdb = get_db()
        error = None

        if not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'
        elif token != current_app.config['REGISTRATION_TOKEN']:
            error = 'Invalid token.'
        elif [user for user in vdb.users if user.username == username]:
            error = 'User {} is already registered'.format(username)

        email = None if email == "" else email

        if error is None:
            user = User(username=username,
                        password=generate_password_hash(password),
                        email=email)
            vdb.users.append(user)
            vdb.commit()
            return redirect(url_for('auth.login'))

        flash(error)

    return render_template('auth/register.html')


@BP.route('/login', methods=('GET', 'POST'))
@BP.route('/', methods=('GET', 'POST'))
def login():
    """
    Login page endpoint
    """
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        vdb = get_db()
        error = None

        if is_empty(username):
            error = 'Invalid username'
        elif is_empty(password):
            error = 'Invalid password'
        else:
            user = vdb.users.find(username)
            if user is None:
                error = 'Incorrect username.'
            elif not check_password_hash(user.password, password):
                error = 'Incorrect password.'

        if error is None:
            redirect_url = request.args.get('referrer') or url_for(
                'blog.index')
            session.clear()
            session['username'] = username
            return redirect(redirect_url)

        flash(error)

    return render_template('auth/login.html')


@BP.route('/reset', methods=('GET', 'POST'))
def reset_start():
    """
    Reset password endpoint
    """
    if request.method == 'POST':
        username = request.form['username']
        usermail = request.form['email']
        vdb = get_db()
        user = vdb.get_user_username(username)

        if user is None:
            flash('User does not exist.')
        elif is_empty(user.email):
            flash('User has no registered email.')
        elif user.email != usermail:
            flash('Email does not match on record')
        else:
            token = token_handler('generate', user.id) if is_empty(
                user.token) else user.token
            send_mail(
                to_address=user['email'],
                subject='Panchayat: Password Reset Link',
                body=
                f"""<html>Open <a href="{current_app.config['ROOT_URL']}"{url_for("auth.login")}{token}/reset">reset link</a> to reset your panchayat password</html>"""  # pylint: disable=line-too-long
            )

    return render_template('auth/reset.html', initiate_reset=True)


@BP.route('/<string:token>/reset', methods=('GET', 'POST'))
def reset_end(token):
    """
    Unique reset url endpoint
    Tokenized url authenticating user to reset password
    """
    if request.method == 'POST':
        # extract fields from form
        username = request.form['username']
        email = request.form['email']
        new_password = request.form['new-password']
        new_password_2 = request.form['new-password-2']

        # validate POST fields for user update
        if is_empty(token):
            flash('Invalid Token')
        elif is_empty(username):
            flash('Username field is empty')
        elif is_empty(email):
            flash('Email field is empty')
        elif is_empty(new_password):
            flash('Password field is empty')
        elif new_password != new_password_2:
            flash('New Passwords Mismatch')
        else:
            vdb = get_db()
            user = vdb.get_user_username(username)

            if not user:
                flash('No such user exists')
            elif user.email != email:
                flash('User email mismatch')
            elif user.token != token:
                flash('User token mismatch')
            else:
                vdb.edit_password(user, generate_password_hash(new_password))
                # reset user's token on successful password reset
                token_handler('generate', user.id)

                return redirect(url_for('auth.login'))

    return render_template('auth/reset.html', initiate_reset=False)


@BP.before_app_request
def load_logged_in_user(force=False):
    """
    Load current logged in user object
    """
    username = session.get('username')

    if not force and username is None:
        g.user = None
    else:
        g.user = get_db().users.find(username)


@BP.route('/logout')
def logout():
    """
    Logout page endpoint
    """
    session.clear()
    return redirect(url_for('index'))


def login_required(view):
    """
    Decorator to mark functions that need login to access
    """
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            referrer = request.args.get('referrer', None)
            return redirect(url_for('auth.login', referrer=referrer))

        return view(**kwargs)

    return wrapped_view


def token_required(view):
    """
    Decorator to mark functions that need tokenized url to access
    """
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user or is_valid_token(request):
            return view(**kwargs)
        referrer = request.args.get('referrer', None)
        return redirect(url_for('auth.login', referrer=referrer))

    return wrapped_view


def mailauth_required(view):
    """
    Decorator to mark webhooks that must be only be called by mailgun
    """
    @functools.wraps(view)
    def wrapped_view(**kwargs):  # pylint: disable=inconsistent-return-statements
        vdb = get_db()
        mailgun_api_key = vdb.users.find('mailgun').token

        token = request.form['token']
        timestamp = request.form['timestamp']
        signature = request.form['signature']

        sender = request.form['From'].split('<')[1].split('>')[0]

        g.user = [user for user in vdb.users if user.email == sender][0]

        if g.user and is_mailgun(mailgun_api_key, token, timestamp, signature):
            return view(**kwargs)
        abort(401)

    return wrapped_view
