#!flask/bin/python
"""
Script of profiling flask.
python3 profile-flask.py
"""

from werkzeug.contrib.profiler import ProfilerMiddleware
from panchayat import create_app

app = create_app()

app.config['PROFILE'] = True
app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[30])
app.run(host="0.0.0.0", debug=True)
